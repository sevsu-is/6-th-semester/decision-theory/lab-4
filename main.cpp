#include <iostream>

const int row = 10, column = 2;
int matrix[row][column] = {
        //   f1       f2
        {3, 2},
        {4, 5},
        {5, 3},
        {8, 3},
        {6, 2},
        {3, 8},
        {6, 4},
        {2, 5},
        {6, 4},
        {2, 5}
};

int matrix_f1[row] = {}, matrix_f1_form[row] = {}, matrix_f2[row] = {}, matrix_f2_form[row] = {};
int i, j, k;

int main() {
    // Перепишем в одномерные массивы значения каждого из критериев
    for (i = 0; i < row; i++) {
        for (j = 0; j < column - 1; j++) {
            matrix_f1[i] = matrix[i][j];
            matrix_f2[i] = matrix[i][j + 1];
        }
    }

    std::cout << "F1: ";
    for (i = 0; i < row; i++) {
        std::cout << matrix_f1[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "F2: ";
    for (i = 0; i < row; i++) {
        std::cout << matrix_f2[i] << " ";
    }
    std::cout << std::endl << std::endl;

    // Убираем доминируемые без эквивалентности
    for (k = 0; k < row; k++) {
        for (i = 1; i < row; i++) {
            bool prevIsAllWorse = matrix_f1[k] < matrix_f1[i] && matrix_f2[k] < matrix_f2[i];
            bool prevIsAllBetter = matrix_f1[k] > matrix_f1[i] && matrix_f2[k] > matrix_f2[i];

            if (prevIsAllWorse) {
                for (j = 0; j < column; j++) {
                    // убрать предыдущий
                    matrix[k][j] = 0;
                }
            } else if (prevIsAllBetter) {
                for (j = 0; j < column; j++) {
                    // убрать текущий
                    matrix[i][j] = 0;
                }
            }
        }
    }

    // Убираем эквивалентности и доминирования
    for (k = 0; k < row; k++) {
        for (i = 1; i < row; i++) {
            bool currIsSameOnF1ButBetterOnF2 = matrix_f1[k] == matrix_f1[i] && matrix_f2[k] < matrix_f2[i];
            bool currIsSameOnF1ButWorseOnF2 = matrix_f1[k] == matrix_f1[i] && matrix_f2[k] > matrix_f2[i];
            bool currIsBetterOnF1AndSameOnF2 = matrix_f1[k] < matrix_f1[i] && matrix_f2[k] == matrix_f2[i];
            bool currIsWorseOnF1AndSameOnF2 = matrix_f1[k] > matrix_f1[i] && matrix_f2[k] == matrix_f2[i];
            bool currIsSameOnBoth = k < i && matrix_f1[k] == matrix_f1[i] && matrix_f2[k] == matrix_f2[i];

            if (currIsSameOnF1ButBetterOnF2 || currIsBetterOnF1AndSameOnF2) {
                for (j = 0; j < column; j++) {
                    // убрать предыдущий
                    matrix[k][j] = 0;
                }
            } else if (currIsSameOnF1ButWorseOnF2 | currIsWorseOnF1AndSameOnF2 | currIsSameOnBoth) {
                for (j = 0; j < column; j++) {
                    // убрать текущий
                    matrix[i][j] = 0;
                }
            }
        }
    }

    // число решений в паретто-границе
    int count = 0;
    for (i = 0; i < row; i++) {
        for (j = 0; j < column; j++) {
            if (matrix[i][j] != 0) {
                count++;
            }
        }
    }
    int count_row = count / 2;

    // Создадим массив, содержащий Парето-границу
    int **Pareto = new int *[count_row];
    for (j = 0; j < count_row; j++)
        Pareto[j] = new int[column];

    k = 0;
    for (i = 0; i < row; i++) {
        for (j = 0; j < column - 1; j++) {
            if (matrix[i][j] != 0) {
                Pareto[k][j] = matrix[i][j];
                Pareto[k][j + 1] = matrix[i][j + 1];
                k++;
            }
        }
    }

    std::cout << "Парето-граница" << std::endl;
    for (i = 0; i < count_row; i++) {
        for (j = 0; j < column; j++) {
            std::cout << Pareto[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    // Определение координат точки утопии
    int max_f1 = 0, max_f2 = 0;
    int f1max_f2[column] = {0, 0}, f2max_f1[column] = {0, 0};

    for (i = 0; i < count_row; i++) {
        for (j = 0; j < column - 1; j++) {
            if (Pareto[i][j] > max_f1) {
                max_f1 = Pareto[i][j];
                f1max_f2[j] = max_f1;
                f1max_f2[j + 1] = Pareto[i][j + 1];
            }
            if (Pareto[i][j + 1] > max_f2) {
                max_f2 = Pareto[i][j + 1];
                f2max_f1[j] = Pareto[i][j];
                f2max_f1[j + 1] = max_f2;
            }
        }
    }

    std::cout << "Точка утопии имеет координаты: (" << max_f1 << ";" << max_f2 << ")" << std::endl;

    std::cout << "f1max; f2" << std::endl;
    for (i = 0; i < column; i++) {
        std::cout << f1max_f2[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "f2max; f1" << std::endl;
    for (i = 0; i < column; i++) {
        std::cout << f2max_f1[i] << " ";
    }
    std::cout << std::endl;

    // Расчет расстояния от точки утопии до рассматриваемого решения
    auto *r_Pareto = new double[count_row];

    for (i = 0; i < count_row; i++) {
        for (j = 0; j < column - 1; j++) {
            r_Pareto[i] = ((max_f1 - Pareto[i][j]) ^ 2 + (max_f2 - Pareto[i][j + 1]) ^ 2) ^ (1 / 2);
        }
    }

    std::cout << std::endl << "r (distance)" << std::endl;
    for (i = 0; i < count_row; i++) {
        std::cout << r_Pareto[i] << " for " << Pareto[i][0] << " " << Pareto[i][1] << std::endl;
    }

    // Минимальное расстояние
    double min = 100, index;
    for (i = 0; i < count_row; i++) {
        if (min > r_Pareto[i]) {
            min = r_Pareto[i];
            index = i;
        }
    }
    std::cout << std::endl << "min = " << min << std::endl;

    for (i = 0; i < count_row; i++) {
        for (j = 0; j < column - 1; j++) {
            if (i == index) {
                std::cout << "Точка с наименьшим расстоянием имеет координаты: (" << Pareto[i][j] << ";" << Pareto[i][j + 1]
                     << ")" << std::endl;
            }
        }
    }
}
